#include <cstdio>
#include <cstdlib>
#include <list>
#include <algorithm>
#include <iostream>

using namespace std;

#define BSIZE 1<<15
unsigned int *parents;
unsigned int *weights;
unsigned long long countNodes(unsigned int *k);
unsigned int find(unsigned int target);
unsigned int my_union(unsigned int u,unsigned int v);

char buffer[BSIZE];
long bpos = 0L, bsize = 0L;

int sorted(const void *a, const void *b) { return (*(unsigned int **)a)[1] - (*(unsigned int **)b)[1]; }

long readLong()
{
    long d = 0L, x = 0L;
    char c;

    while (1)  {
        if (bpos >= bsize) {
            bpos = 0;
            if (feof(stdin)) return x;
            bsize = fread(buffer, 1, BSIZE, stdin);
        }
        c = buffer[bpos++];
        if (c >= '0' && c <= '9') { x = x*10 + (c-'0'); d = 1; }
        else if (d == 1) return x;
    }
    return -1;
}
unsigned int readInt()
{
    unsigned int k;
    fread(&k, sizeof(unsigned int), 1, stdin);
    return k;
}
int main(void)
{
    unsigned int N;
    unsigned int a,b,w;
    unsigned int *k;
    N = (unsigned int) readLong();
    unsigned long long result = 0ULL;
    unsigned int **edges = new unsigned int*[N-1];
    weights = new unsigned int[N];
    parents = new unsigned int[N];

	fill(weights,weights+N,1);
	for(unsigned int i = 0; i <= N - 1; i++)
		parents[i] = i;
		 
    for(unsigned int i = 0; i < N - 1; i++)
    {
        a = (unsigned int) readLong();
        b = (unsigned int) readLong();
        w = (unsigned int) readLong();
        a--;
        b--;
        k = new unsigned int[3];
        k[0] = b;
        k[1] = w;
        k[2] = a;

        result += w;
        edges[i] = k;
    }
    qsort(edges, N-1, sizeof(unsigned int *), sorted);
    
    for(unsigned int i = 0; i < N - 1; i++)
    {
        result += countNodes(edges[i])*(edges[i][1]+1);
    }
    printf("%llu\n", result);

    return 0;
}

unsigned int find(unsigned int target)
{
	if(target != parents[target])
		parents[target]=find(parents[target]);
	return parents[target];
}

unsigned int my_union(unsigned int u,unsigned int v)
{
	parents[v]=u;
	return u;
}

unsigned long long countNodes(unsigned int *k)
{
    unsigned long long count;
    unsigned long long countRem;
	unsigned int u, v;
	unsigned int father_u, father_v;
	
	u = k[0];
	v = k[2];
	
	father_u = find(u);
	father_v = find(v);
	count = weights[father_u];
	countRem = weights[father_v];
	//printf("nodes: %u %u fathers: %u %u, neighs: %llu %llu\n",u,v,father_u,father_v,count,countRem);
	u=my_union(father_u,father_v);
	weights[u]=count + countRem;
	
    return count*countRem-1;
}


