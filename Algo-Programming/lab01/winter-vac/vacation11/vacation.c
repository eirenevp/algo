/*Vlassi-Pandi Eirini (03108137)*/
/*        ex1.1 algo 2011-12   */


#include <stdlib.h>
#include <stdio.h>


void merge(long long int*, int, int, int);
void mergeSort(long long int*, int, int); 

int main() {
  int N,C,i=0,sol=0,c=0,max,k;

  scanf("%d %d",&N,&C);  
  int *A = calloc(N,sizeof(int));
  if (A == NULL) exit(1);
  for (i=0;i<N;i++) {
        scanf("%d",A+i);
        if (A[i]-C > 0){c++;} 
  }

  long long int *S = calloc(2*N,sizeof(long long int));
  if (S == NULL) exit(1);
  S[0] = A[0]-C;
  for (i=2;i<2*N;i++) {
        S[i] = S[i-2]+A[i/2]-C;
        if (S[i] >= 0) {sol= (i+2)>>1;}
        S[i-1]= i >> 1;i++;
  }
  S[2*N-1] = N;
  
  mergeSort(S,0,N-1);
  
  for (i=1;i<2*N;i+=2){
        A[i>>1]=S[i]; 
  }  
  
  for (i=0;i<N;i++) {
        S[i]=0;
  }
  k = 0;
  max= A[0];
  for (i=1;i<N;i++) {
        if (A[i] < max) { 
            if (S[k] <= max-A[i]) {S[k]=max-A[i];}}
        else {max=A[i];k++;}
  }      
  
  max = S[0];
  for (i=1;i<N;i++) {
        if (S[i] > max) {max=S[i];} 
  }
  if ( max > sol) {sol=max;}
  if (sol > 0) {printf("%d\n",sol);}
  else if (c > 0){ 
    printf("1\n");
  }
  else printf("0\n"); 
  free(A);
  free(S);
  return 0;
}

void merge(long long int* S, int low, int mid, int up) {
  up = up<<1;up++;
  mid = mid<<1;
  low = low<<1;
  int xmid = mid-low, xup = (up-low),q;
  int i = 0, j = xmid+2, k = low,nlow=low;
  long long int *X = calloc((up-low+1),sizeof(long long int));
  if (X == NULL) exit(1);

  for (i=0;i<(up-low+1);i++) {
        X[i] = S[nlow+i];
  }
  
  i=0; //!!
  while ((i <= xmid) && (j <= xup)) {
    if (X[i] > X[j]) {S[k++] = X[i++]; S[k++] = X[i++];}
    else if (X[i] < X[j]) {S[k++] = X[j++]; S[k++] = X[j++];}
    else 
     if ( X[i+1]> X[j+1]) {S[k++] = X[i++]; S[k++] = X[i++];}
     else {S[k++] = X[j++]; S[k++] = X[j++];}
  }
  if (i > xmid)
    for (q=j;q<=xup-1;q+=2){
          S[k++] = X[q];
          S[k++] = X[q+1];
    }
  else
    for (q=i;q<=(xmid);q+=2){
          S[k++] = X[q];
          S[k++] = X[q+1];
    }     
  free(X);
}
    
void mergeSort(long long int* S, int left, int right) {
  int mid;
  if (left >= right) return;
  mid= (left+right)/2;
  mergeSort(S,left,mid);
  mergeSort(S,mid+1,right);
  merge(S,left,mid,right);
} 
