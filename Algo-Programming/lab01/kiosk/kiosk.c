/* Irene Vlassi-Pandi (03108137) */
/*      ex1.1 algo 2012-13       */


#include <stdlib.h>
#include <stdio.h>
#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })

int main() {
  long long int N,K,size,new,times,i;
  long long int upperBound,lowerBound,bound,move,middle=0;
  int  flag;

  scanf("%ld %ld", &N, &K); 
  size = N<<1; 
  long int *pos = calloc(size, sizeof(long int)); 
  if (pos == NULL) exit(1); 
  for (i=0;i<size;i+=2) { 
      scanf("%ld %lld/n", &pos[i], &pos[i+1]); 
  }
 
 flag = 0;
 bound = 0;
 while(!flag){
   flag = 1;
   new = pos[0] - bound;
   for (i=0;i<size;i+=2) {
       if (i == 0) times = pos[1] - 1;
       else times = pos[i+1];
       while (times > 0) {
           move = max((pos[i]-bound), (new+K)) - pos[i];
           if (abs(move) <= bound) {
               new = pos[i] + move;  
               times--;
           } 
           else {
           flag = 0; break;
           } 
       }
       if (!flag) break;
   }
   if (bound == 0 && flag == 1) bound = 0;
   else if (bound == 0 && flag == 0) bound = 2;
   else bound <<= 1;
 }
 
  upperBound = bound >> 1;
  lowerBound = bound >> 2;
  printf("Bound %lld\n", bound);
 // lowerBound = 0;
 // upperBound = 2*N*K;
  printf("Bound %lld\n", bound);
  printf("uBound %lld\n", upperBound);
  printf("PBound %lld\n", lowerBound);
//  upperBound = bound;
//  lowerBound = bound >> 1;
//  
//  for (i=0;i<size;i+=2) { 
//      pos[i] <<= 1;
//  }
  
//  K <<= 1;
  while (lowerBound <= upperBound) {
      middle = (lowerBound + upperBound)/2;
      flag = 1;
      new = pos[0] - middle;
      for (i=0;i<size;i+=2) {
          if (i == 0) times = pos[1] - 1;
          else times = pos[i+1];
          while (times > 0) {
          move = max((pos[i]-middle), (new+K)) - pos[i];
          if (abs(move) <= middle) {
              new = pos[i] + move;  
              times--;
          } 
          else { 
              flag = 0; break;
          } 
        }
        if (!flag) break;
      } 
      if (flag == 0) lowerBound = middle+1;
	  else upperBound = middle-1;
   } 
 
  printf("%lld", middle);
  //printf("%.2f\n", (1.0*middle));
  //printf("%.2f\n", (1.0*lowerBound/2));
  free(pos);

  return 0;
}

