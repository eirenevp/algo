#include <stdio.h>
#include <stdlib.h>


int main() {

	int flag;
	long int N, result, counter1, i=0, j, C, s=0, b=0, max=0;
	scanf("%ld %ld", &N, &C);
	long int *T = calloc(N,sizeof(long int));
	if (T==NULL) exit(1);
	for (i=0;i<N;i++){
		scanf("%ld",T+i);
	}

	long long int *S = calloc(N, sizeof(long long int));
	if (S==NULL) exit(1);
	long long int *small =  calloc(N,sizeof(long long int));
	if (small==NULL) exit(1);
	long long int *big = calloc(N,sizeof(long long int));
	if (big==NULL) exit(1);
	
	S[0]=T[0]-C;
	if (S[0]>=0) counter1=1;
	else counter1=0;
	for (i=1;i<N;i++) {
		S[i]=S[i-1]+T[i]-C;
		if (S[i]>=0) counter1=i+1;
	}
	
    small[0]=0;
	big[0]=N-1;
	for (i=1;i<N;i++){
		if (S[i]<S[small[s]]) {s++; small[s]=i;}
		if (S[N-i-1]>S[big[b]]) {b++; big[b]=N-i-1;}
	}
	
	i=0;
	while (i<=b){
		j=s;flag=0;
		while ((j>=0)&&(flag==0)){
			if (S[big[i]]>=S[small[j]]) {
				if ((big[i]-small[j]>=max)) {max=big[i]-small[j];}
				j--;
			}
			else {s=j;flag=1;}
		}
		i++;	

	}

	free(T);
	free(S);
	if (max>=counter1) 
		result=max;
	else 
		result=counter1;
	printf("%ld\n",result);
	return 0;
}
