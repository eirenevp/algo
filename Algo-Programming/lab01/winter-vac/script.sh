#!/bin/bash
gcc winter-vac.c -o winter-vac &&
for i in {1..9}
do
    echo "Running test $i..."
    ./winter-vac < "input$i.txt" > "myoutput$i.txt"
    diff -q "myoutput$i.txt" "output$i.txt" 
done

