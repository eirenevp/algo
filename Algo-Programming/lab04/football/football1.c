#include <stdio.h>

long long int nod,winner,top,strong,indexing,n,m,k,l,i,j,v,u,count;
long long int Strongly[30010];
long long int Stack[30010];
long long int S[30010];
long long int Index[30010];
long long int LowLnk[30010];

struct node 
{
    long long int ant_omada;
    struct node *next;  
};

struct node Akmes[30010];
struct node *temp;
struct node *temp2;

long long int explore(long long int ); 
long long int pop();
void push(long long int ); 
void strconn(long long int ); 

int main() {

    scanf("%lld", &n);
    for (k=1;k<=n;k++) {
        Akmes[k].next = NULL;
        Index[k]=-1;
        S[k]=0;
        Strongly[k] = 0;
    }

    for (i = 1; i <= n; ++i) {
        scanf("%lld", &m);

        for (j = 0; j < m; ++j) {
            scanf("%lld", &winner);

            temp=(struct node *)malloc(sizeof(struct node));
            temp->ant_omada=i;
            temp->next=Akmes[winner].next;
            Akmes[winner].next=temp;  
        }
    }

    top = 0;
    strong = 0;
    indexing = 0;

    for (i=1;i<=n;i++) {
        if (Index[i] == -1) {
            strconn(i);
        }
    }

    count = 0;

    for (i=1;i<=n;i++) {
        if (Strongly[i] == strong) {
            count++;
        }
    }
    
    printf("%lld\n", count);

    return 0;
}

long long int explore(long long int komvos) {
    temp = &Akmes[komvos];
    temp2 = temp->next;
    if (temp2 != NULL) {
        nod = temp2->ant_omada;
        temp->next = temp2->next;
    }
    else {
        nod = -1;
    }
    return nod;
}

long long int pop(void) {
    u = Stack[top];
    top--;
    S[u] = 0;
    return u;
}

void push(long long int v) {
    top++;	
    Stack[top] = v;
    S[v] = 1;
}

void strconn(long long int v) {
    long long int w;

    Index[v] = indexing;
    LowLnk[v] = indexing;
    indexing++;
    push(v);

    while ((w = explore(v)) != -1) {
        if (Index[w] == -1) {
            strconn(w);
            if (LowLnk[w] < LowLnk[v]) 
                LowLnk[v] = LowLnk[w];
        }
        else if (S[w] == 1) {
            if (Index[w] < LowLnk[v]) 
                LowLnk[v] = Index[w];
        } 
    }

    if (LowLnk[v] == Index[v]) {
        strong++;
        do {
            w = pop();
            Strongly[w] = strong;
        } while (w != v);
    }
}


