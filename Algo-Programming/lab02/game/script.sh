#!/bin/bash
for i in {1..25}
do
    echo "Running test $i..."
    ./game < "input$i.txt" > "myoutput$i.txt"
    diff -q "myoutput$i.txt" "output$i.txt" 
done

