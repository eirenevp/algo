#!/bin/bash
gcc boats.c -o boats &&
for i in {1..12}
do
    echo "Running test $i..."
    ./boats < "input$i.txt" > "myoutput$i.txt"
    diff -q "myoutput$i.txt" "output$i.txt" 
done

