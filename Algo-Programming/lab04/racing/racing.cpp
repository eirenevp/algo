#include <cstdio>
#include <cstdlib>
#include <queue>
#include <list>
#include <cstring>
#include <algorithm>
#include <cassert>
#define mp make_pair 

using namespace std;

priority_queue< pair<long long, int>, vector< pair<long long, int> >, greater< pair< long long, int > > > heap;
vector< list< pair<int, long long> > > edge;

bool vis[60010];
long long dist[60010];
int T[60010];

int main(void) {
    int N, M, K, L, B, u, v, i, C;
    long long x;
    long long ans = 0;

    scanf("%d %d %d %d %d", &N, &M, &K, &L, &B);

    edge.resize(N);
    for (i=0; i<M; ++i) {
        scanf("%d %d %lld", &u, &v, &x);
        u -= 1; v -= 1;

        edge[u].push_front(mp(v, x));
        edge[v].push_front(mp(u, x));
    }

    memset(dist, 0x3f, sizeof(dist));

    for (i=0; i<K; ++i) {
        scanf("%d", &T[i]);
        T[i]-=1;
    }

    for (i=0; i<B; ++i) {
        scanf("%d", &C);
        C-=1;
        heap.push(mp(0,C));
        dist[C] = 0;
    }

    while(!heap.empty()) {
        pair<long long, int> cur = heap.top();
        heap.pop();
        
        int curnode = cur.second;

        if ( vis[curnode] ) continue;
        vis[curnode] = true;
        assert(dist[curnode] == cur.first);

        for (list<pair<int, long long> >::iterator i = edge[curnode].begin(); i != edge[curnode].end(); ++i) {
            int next = (*i).first;
            long long weight = (*i).second;

            assert(next >= 0 && next < N);
            assert ( !vis[next] || dist[next] <= weight + dist[curnode] );

            if ( !vis[next] && dist[next] > weight + dist[curnode] ) {
                dist[next] = dist[curnode] + weight;
                heap.push(mp(dist[next], next));
            }
        }
    }

    //for (i=0; i<N; ++i) {
    //    if ( !vis[i] ) printf("%d\n", i);
    //    assert(vis[i] == 1);
    //}

    for (i=0; i+1<K; ++i) {
        for (list< pair<int, long long> >::iterator j = edge[T[i]].begin(); j != edge[T[i]].end(); ++j) {
            if ( (*j).first == T[i+1]) {
                ans += (*j).second;
                break;
            }
        }
    }
    
    vector<long long> D;

    for (i=1; i+1<K; ++i) {
        D.push_back(dist[T[i]]);
    }

    sort(D.begin(), D.end());
    
    for (i=0; i<L; ++i) {
        ans += D[i];
    }

    printf("%lld\n", ans);

    return 0;
}
