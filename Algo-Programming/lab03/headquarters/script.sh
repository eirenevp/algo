#!/bin/bash
for i in {1..14}
do
    echo "Running test $i..."
    ./headquarters < "input$i.txt" > "myoutput$i.txt"
    diff -q "myoutput$i.txt" "output$i.txt" 
done

