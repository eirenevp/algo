#include <stdio.h>

#define GRAPHSIZE 60010

struct node 
{
long long int second;
long long int weight;
struct node *next;  /*black pointer*/
};

struct node Deiktes[GRAPHSIZE];
struct node *temp;
struct node *temp2;
struct node *temp_previous;
long long int n,m,k,l,start,w,last,athroisma,result,i,weight,out,stop,keep,ma,hs,p,r,max,mp,g,gg,tempos;
long long int Plithos[GRAPHSIZE];
long long int d[GRAPHSIZE];
long long int A[GRAPHSIZE][3];
long long int B[GRAPHSIZE];


void insert(long long int k, long long int thesi) {

long long int i,p;

	hs++;	
	A[hs][1] = k;
	A[hs][2] = thesi;
	B[thesi] = hs;
	i = hs; 
	p = i / 2;
	
	while ((i > 1) && (A[p][1] > A[i][1])) {
		tempos = A[p][1];
		A[p][1]=A[i][1];
		A[i][1] = tempos;
		tempos = A[p][2];
		A[p][2]=A[i][2];
		A[i][2] = tempos;	
		B[A[p][2]] = p;
		B[A[i][2]] = i;		
		i = p; 
		p = i / 2;
	} 
}

void valto(long long int thesi) {

long long int i,p;


	i = thesi; 
	p = i / 2;
	
	while ((i > 1) && (A[p][1] > A[i][1])) {
		tempos = A[p][1];
		A[p][1]=A[i][1];
		A[i][1] = tempos;
		tempos = A[p][2];
		A[p][2]=A[i][2];
		A[i][2] = tempos;	
		B[A[p][2]] = p;
		B[A[i][2]] = i;		
		i = p; 
		p = i / 2;
	} 
}




void combine(long long int i) {

long long int l,mp,r;

	l = 2*i; r = 2*i+1; mp = i;
	if ((l <= hs) && (A[l][1] < A[mp][1]))
		mp = l;
	if ((r <= hs) && (A[r][1] < A[mp][1]))
		mp = r;
	if (mp != i) {
		tempos = A[i][1];
		A[i][1] = A[mp][1];
		A[mp][1] = tempos;
		tempos = A[i][2];
		A[i][2] = A[mp][2];
		A[mp][2] = tempos;
		B[A[mp][2]] = mp;
		B[A[i][2]] = i;
		combine(mp); 
	} 
	
}


void constructHeap(long long int n) {

long long int i;	

	hs = n;
	for (i = n / 2; i > 0; i--)
		combine(i);
}



long long int deleteMax() {
	
	if (hs == 0){
		return(1);
	}
	max = A[1][2];
	
	A[1][1] = A[hs][1];
	A[1][2]	= A[hs][2];
	B[A[hs][2]] = 1;
	hs--;
	if (hs != 0)
		combine(1);
	return(max); 
}





void explore(long long int komvos) {

	temp = Deiktes[komvos].next;	
	while (temp != NULL) {
		if ( (temp->weight + d[komvos] < d[temp->second]) || (d[temp->second] == -1) ) {
			keep = d[temp->second];			
			d[temp->second] = temp->weight + d[komvos];
			if (keep == -1) {
				g = d[temp->second];
				gg = temp->second;
				insert(g, gg);
			}
			else {				
				A[B[temp->second]][1] = d[temp->second];
				valto(B[temp->second]);
			}
		}
		temp = temp->next;
	}
}	

 		
void dijkstra(long long int s, long long int l) {
	long long int q2;

	d[s] = 0;

	
	hs = 0;
	temp = Deiktes[s].next;	
	while (temp != NULL) {
		
	if ( (temp->weight + d[s] < d[temp->second]) || (d[temp->second] == -1) ) {
		keep = d[temp->second];
		d[temp->second] = temp->weight + d[s];

	if (keep == -1) {
		hs++;
		A[hs][1] = d[temp->second];
		A[hs][2] = temp->second;
		B[temp->second] = hs; 
	}
	else {				
		A[B[temp->second]][1] = d[temp->second];
	}
	
	}

		temp = temp->next;
	}
	
	constructHeap(hs);	
	q2 = deleteMax();

	while (q2 != l) {				
		explore(q2);
		q2 = deleteMax();
	}
}



int main(void) {
	
	scanf("%lld", &n);
	scanf("%lld", &m);

	for (k=1;k<=n;k++) {
		Deiktes[k].next = NULL;
		d[k] = -1;

	}

	athroisma = 0;
		
	for (i = 0; i < m; ++i) {
		
		scanf("%lld", &k);
		scanf("%lld", &l);
		scanf("%lld", &weight);
		
		temp=(struct node *)malloc(sizeof(struct node));
		temp->second=l;
		temp->weight=weight;
//		temp2 = &Deiktes[k-1];
		temp->next=Deiktes[k].next;
		Deiktes[k].next=temp;  /* pointing to the first node */
			
		temp=(struct node *)malloc(sizeof(struct node));
		temp->second=k;
		temp->weight=weight;
		
		temp->next=Deiktes[l].next;
		Deiktes[l].next=temp;  /* pointing to the first node */
		
		athroisma = athroisma + weight;

		Plithos[l]++;
		Plithos[k]++;
	}


	for (i=1;i<=n;i++) { 
		if (Plithos[i] % 2 == 1) {
			start = i;
			break;
		}
	}	
		
	for (i=1;i<=n;i++) { 
		if (Plithos[i] % 2 == 1) {
			if (start != i) {
				last = i;
				break;
			}
		}
	}

	dijkstra(start, last);

	out = d[last] + athroisma ;

	printf("%lld\n", out);

	return 0;
}

