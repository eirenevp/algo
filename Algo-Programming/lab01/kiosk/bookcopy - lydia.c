#include <stdio.h>
#include <stdlib.h>

//Lydia Zakynthinou (am 03108024)

int main(){
	long int l, r, middle, sum, writers, total=0, max=0, N, k, i, minimum, mid;
	scanf("%ld %ld", &N, &k);
	long int* pages = calloc(N,sizeof(long int));
	
	for (i=0;i<N;i++){
		scanf("%ld\n",&pages[i]);
	}
	
	for (i=0;i<N;i++){
		total=total+pages[i];
		if (pages[i]>max) max=pages[i];
	}

	if  ((total%k)>0) mid = total/k+1;
	else mid = total/k;
	if (mid>=max) minimum=mid;
	else minimum=max;

	l=minimum; r=total;

	while (l<=r){
		if (((l+r)%2)>0) middle = (l+r)/2 +1;
		else middle = (l+r)/2;
		sum=0;
		writers=1;
		for (i=0;i<N;i++){
			if ((sum+pages[i])<=middle) sum=sum+pages[i];
			else {sum=pages[i];writers++;}
		}
		if (writers>k) l=middle+1;
		else r=middle-1;
	}
	printf("%ld\n",l);
	free(pages);
	return 0;
}
