#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <algorithm>

using namespace std;

int n;
long long ind[100010];
long long howmany[100010];
long long k;

int can(long long t) {
    int i;
    long long prev;

    prev = (ind[0] - t) + k*(howmany[0]-1);
    if ( prev - ind[0] > t ) return 0;

    for (i=1; i<n; ++i) {
        prev = max(ind[i] - t, prev + k);
        if ( prev - ind[i] > t ) break;
        prev = prev + k*(howmany[i]-1);
        if ( prev - ind[i] > t ) break;
    }
    
    return (i == n);
}

int main(void) {
    int i, j;
    long long lo, hi, mid;
    long long a, b;

    scanf("%d %lld", &n, &k);

    k <<= 1;

    for (i=0; i<n; ++i) {
        scanf("%lld %lld", &a, &b);
        ind[i] = a*2;
        howmany[i] = b;
    }

    lo = 0; hi = 20000000000010LL;
    while (lo != hi) {
        mid = (lo + hi)>>1;
        if ( can(mid) ) hi = mid;
        else lo = mid+1;
    }

    printf("%.2lf\n", lo/2.0);

    return 0;
}
