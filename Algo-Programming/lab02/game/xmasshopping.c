#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Lydia Zakynthinou (am 03108024)

int d(int* start, int* dest){
	return (abs(dest[0]-start[0])+abs(dest[1]-start[1]));

}

int** make2darray(int x, int y){
	int** myarray;
	int l;
	myarray = (int** )malloc(x*sizeof(int*));
	for (l=0;l<x;l++){
	myarray[l] = (int* )malloc(y*sizeof(int));
	}
	return myarray;

}


int main() {

	int N, R, C, i, j, t, k, min, minimum, ni;
	scanf("%d %d %d", &N, &R, &C);
	ni=N+2;
	int** points = make2darray(ni,2);
	if (points==NULL) exit(1);
	for (i=0;i<ni;i++){
		scanf("%d %d", &points[i][0], &points[i][1]);
	}	
	int** T = make2darray(N+1,N+1);
	if (T==NULL) exit(1);

	//arxisame pali
	//for (i=0;i<ni;i++){printf("Point%d (%d,%d)\n",i,points[i][0],points[i][1]);}

	//T[0][1]=d(points[1],points[2]);
	//T[1][0]=d(points[0],points[2]);
	//pouf
	//printf("T[0][1]=%d , T[1][0]=%d\n", T[0][1],T[1][0]);
	for(i=0;i<=N;i++){
		for(j=0;j<=N;j++){
			if (i<(j-1)) T[i][j]=T[i][j-1]+d(points[j],points[j+1]);
			else if (i==(j-1)) {
				if (i==0) min=d(points[1],points[2]);
				else {
					min = T[0][i]+d(points[0],points[j+1]);
					for (k=1;k<i;k++){
						t=T[k][i]+d(points[k+1],points[j+1]);
						if (min>t) min=t;
					}
				}
				T[i][j]=min;
			}
			else if (i==j) T[i][j]=0;
			else if (i==(j+1)) {
				if (j==0) min = d(points[0],points[2]);
				else {
					min = T[j][0]+d(points[1],points[i+1]);
					for (k=1;k<j;k++){
						t=T[j][k]+d(points[k+1],points[i+1]);
						if (min>t) min=t;
					}
				}	
				T[i][j]=min;
			}
			else T[i][j] = T[i-1][j]+d(points[i],points[i+1]);
		}	
	}
	/*gia na teleiwnoume
	printf("ooolos o pinakas\n");
	for (i=0;i<=N;i++){
		for (j=0;j<=N;j++){
			printf(" %d ", T[i][j]);
		}
	printf("\n");
	}
	*/
	minimum = T[N][0];
	for(j=1;j<N;j++){
		if (minimum > T[N][j]) minimum=T[N][j];
	}
	for (i=0;i<N;i++){
		if (minimum > T[i][N]) minimum=T[i][N];
	}

	printf("%d\n", minimum);
	return 0;
}
