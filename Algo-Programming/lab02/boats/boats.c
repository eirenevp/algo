#include <stdio.h>
#include <stdlib.h>


int** make2darray(int x, int y){
	int** myarray;
	int l;
	myarray = (int** )malloc(x*sizeof(int*));
	for (l=0;l<x;l++){
		myarray[l] = (int* )malloc(y*sizeof(int));
	}
	return myarray;
}

void merge(int** A, int low, int mid, int up) {
	int xmid = mid-low, xup = up-low,q;
	int i = 0, j = xmid+1, k = low;
	int** C = make2darray(xup+1,2);
	if (C == NULL) exit(1);
  
	for (i=0;i<xup+1;i++) {
		C[i][0] = A[low+i][0];
        	C[i][1] = A[low+i][1];
  	}

  	i=0;
  	while ((i <= xmid) && (j <= xup)) {
    		if (C[i][0] < C[j][0]) {A[k][0] = C[i][0]; A[k++][1] = C[i++][1];}
    		else if (C[i][0] > C[j][0]) {A[k][0] = C[j][0]; A[k++][1] = C[j++][1];}
    		else 
      			if (C[i][1] > C[j][1]) {A[k][0] = C[j][0]; A[k++][1] = C[j++][1];}
    			else {A[k][0] = C[i][0]; A[k++][1] = C[i++][1];}
  	}
  
  	if (i > xmid)
    	for (q=j;q<=xup;q++){
          	A[k][0] =  C[q][0]; A[k++][1] = C[q][1];
    	}
  	else
    		for (q=i;q<=xmid;q++){
          	A[k][0] =  C[q][0]; A[k++][1] = C[q][1];
    	}     
  	free(C);
}
    
void mergeSort(int** A, int left, int right) {
	int mid;
  	if (left >= right) return;
  	mid= (left+right)/2;
  	mergeSort(A,left,mid);
  	mergeSort(A,mid+1,right);
  	merge(A,left,mid,right);
}




int main() {
	
	int N,i,L=0, l, r, middle, j;
	scanf("%d\n",&N);
	int** AB = make2darray(N+1,2);
	if (AB==NULL) exit(1);
	AB[0][0]=0; AB[0][1]=0;
	for (i=1;i<=N;i++){
		scanf("%d %d", &AB[i][0], &AB[i][1]);
	}
	int* M = (int* )calloc(N+1,sizeof(int));
	if (M==NULL) exit(1);
	mergeSort(AB,0,N);
	
	for (i=1;i<=N;i++){
		if ((L==0)||(AB[M[1]][1]>AB[i][1])) j=0;
		else {
			l=1;r=L+1;
			while (l<r-1){
				middle = (l+r)/2;
				if (AB[M[middle]][1]<=AB[i][1]) l=middle;
				else r=middle;
			
			}
			j=l;
		}
		if ((j==L)||(AB[i][1]<AB[M[j+1]][1])){
			M[j+1] = i;
			if (L<j+1) L=j+1;
		}
	}
	printf("%d\n",L);
	free(AB);
	free(M);
	return 0;
}
