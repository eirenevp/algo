#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>

#define pb push_back

using namespace std;
typedef long long ll;

struct edge {
    int a, b, cost;
} e;

vector<edge> edges;
int par[500010], n[500010], rank[500010];

bool comp (edge a, edge b) {
    return a.cost < b.cost;
}

int Find_Set (int x) {
    if (par[x] != x) {
        par[x] = Find_Set ( par[x] );
    }
    return par[x];
}

void Link (int a, int b) {
    a = Find_Set ( a );
    b = Find_Set ( b );
    if (rank[a] > rank[b]) {
        par[b] = a;
        n[a] += n[b];
    }
    else {
        par[a] = b;
        n[b] += n[a];
        if (rank[a] == rank[b]) {
            ++rank[b];
        }
    }
}

int main(void) {
    int i, N;
    ll ans, n1, n2;

    scanf("%d", &N);

    ans = 0;
    edges.clear();
    for (i=0; i<N-1; ++i) {
        scanf("%d %d %d", &e.a, &e.b, &e.cost);
        e.a--; e.b--;
        edges.pb(e);
    }
    sort (edges.begin(), edges.end(), comp);

    for (i=0; i<N; ++i) {
        rank[i] = 0;
        par[i] = i;
        n[i] = 1;
    }

    for (i=0; i<N-1; ++i) {
        n1 = n[Find_Set (edges[i].a)];
        n2 = n[Find_Set (edges[i].b)];
        Link (edges[i].a, edges[i].b);
        ans += (ll)n1*n2*(edges[i].cost+1LL) - 1LL;
    }
    printf("%lld\n", ans);

    return 0;
}
