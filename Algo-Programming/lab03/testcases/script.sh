#!/bin/bash
for i in {1..30}
do
    echo "Running test $i..."
    time (./testcases-stergios < "input$i.txt" > "myoutput$i.txt")
    diff -q "myoutput$i.txt" "output$i.txt" 
done

