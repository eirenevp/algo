#include <stdio.h>
#include <stdlib.h>
#define BSIZE 1<<15

char buffer[BSIZE];
long bpos = 0L, bsize = 0L;

long readLong() {
	long d = 0L, x = 0L;
	char c;

	while (1)  {
		if (bpos >= bsize) {
			bpos = 0;
			if (feof(stdin)) return x;
			bsize = fread(buffer, 1, BSIZE, stdin);
		}
		c = buffer[bpos++];
		if (c >= '0' && c <= '9') { x = x*10 + (c-'0'); d = 1; }
		else if (d == 1) return x;
	}
	return -1;
}


struct node {
	int team;
	struct node *next;
};

struct node * new( struct node *head, int tm) {
	struct node *temp = (struct node *) malloc(sizeof(struct node));
	temp->team = tm;
	temp->next = head;
	return temp;
};

struct queue{
	struct node *first;
	struct node *last;
};

void enqueue(int v, struct queue *q){
	struct node *temp = (struct node *) malloc(sizeof(struct node));
	temp->team = v;
	temp->next = NULL;
	if (q->first){
		q->last->next = temp;
		q->last = temp;
	}
	else{
		q->first = temp;
		q->last = temp;
	}
};

int dequeue(struct queue *q){
	struct node *temp = q->first;
	int a = temp->team;
	q->first = temp->next;
	if ((q->first) == NULL) q->last = NULL;
	free(temp);
	return a;
};

long N;
long bfs(int s, struct node **aList) {
	struct node *nptr;
	struct queue *Q = (struct queue *) malloc(sizeof(struct queue));
	int u;
	long cnt=0;
	int *visited = (int *) calloc(N+1, sizeof(int));
	Q->first = NULL;
	Q->last = NULL;
	visited[s] = 1;
	enqueue(s, Q);
	while (Q->first){
		u = dequeue(Q);
		nptr = aList[u];
		while (nptr){
			if (visited[nptr->team] == 0) {
				enqueue(nptr->team, Q);
				visited[nptr->team] = 1;
				cnt++;
			}
			nptr = nptr->next;
		}
			
	}
	
	if (cnt == (N-1)) return 1;
	else return 0;
}

int main() {
	
	int i, j, vert;
	long h, sum=0;
	N = readLong();
	struct node **adjList = (struct node **) malloc((N+1)*sizeof(struct node *));
	for(i=1; i<=N; ++i) adjList[i] = NULL;
	
	for(i=1; i<=N; ++i) {
		h = readLong();
		for (j=0; j<h; ++j) {	
			vert = readLong();
			adjList[vert] = new(adjList[vert], i);
		}
	}

	for(i=1; i<=N; ++i) {
		sum += bfs(i, adjList);
	}
	printf("%ld\n", sum);
		
	return 0;
}
