/* Irene Vlassi-Pandi (03108137)
 *       algo12-13 2.2
 *          game
 */

#define min(a, b) \
   ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
       _a < _b ? _a : _b; })

#include <stdio.h>
#include <stdlib.h>

long int **make2darray(int x, int y) {
	long int **myarray;
    int l;
	myarray = (long int **)malloc(x*sizeof(long int *));
	for (l=0; l<x; l++) {
        myarray[l] = (long int *)malloc(y*sizeof(long int));
	}
	return myarray;
}

int main() {

    int N, i, j, k, nj, maxI;
    long int lmin, maxProfit;
    scanf("%d", &N);
    long int *profits;
    profits = (long int *)malloc(N*sizeof(long int));
    if (profits == NULL) exit(1);

    for (i=0; i<N; i++) {
		scanf("%ld", &profits[i]);
	}	
    
    long int *sum;
    sum = (long int *)malloc((N+1)*sizeof(long int));
    if (sum == NULL) exit(1);
    
    sum[0] = 0;
    for (i=1; i<=N; i++) {
        sum[i] = sum[i-1] + profits[i-1];
    }
    

    long int **bastion = make2darray(N+3, N+3);
    if (bastion == NULL) exit(1);
    
    long int **dp = make2darray(N+3, N+3);
    if (dp == NULL) exit(1);
 
   for (j=1; j<N; j++) {
       nj = j;
       for (i=0; i<N-1; i++) {
           if (nj == N) break;
           if (nj == i) {
               dp[i][nj] = 0;
               bastion[i][nj] = i;
           }
           else if (nj == i+1) {
               dp[i][nj] = min(profits[i], profits[nj]);
               bastion[i][nj] = i;
           }
           else {
               maxProfit = -1;
               maxI = bastion[i][nj-1];
                for (k=maxI; k<=bastion[i+1][nj]; k++) {
                    lmin = min(dp[i][k] + (sum[k+1] - sum[i]), dp[k+1][nj] + (sum[nj+1] - sum[k+1])); 
                    if (maxProfit < lmin) {
                        maxProfit = lmin;
                        maxI = k;
                    }
               }
               dp[i][nj] = maxProfit;
               bastion[i][nj] = maxI;
           }
           nj++;
       }
   }
    
    printf("%ld\n", dp[0][N-1]);	

    
    free(profits);
    free(sum);
    free(dp);

    return(0);
}

