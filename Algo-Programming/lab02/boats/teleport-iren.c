#include <stdlib.h>
#include <stdio.h>


void merge(int**, int, int, int);
void mergeSort(int**, int, int); 
int** array2d(int, int);


int main() {
  int N,i;

  scanf("%d\n",&N);  
  int** AB = array2d(N,2);
  if (AB == NULL) exit(1);
  for (i=0;i<N;i++) {
        scanf("%d %d", &AB[i][0], &AB[i][1]);
  }
/*  for (i=0;i<N;i++) {
        printf("%d  ",A[i]); 
  }
  printf("\n");
  */
/*  int* = malloc(N*sizeof(int));
  if (S == NULL) exit(1);
  S[0] = A[0]-C;
  for (i=1;i<N;i++) {
        S[i] = S[i-1]+A[i]-C;
        if (S[i] >= 0) {c = i+1;}
  }

  for (i=0;i<N;i++) {
        printf("%d  ",S[i]); 
  }*/
  
  printf("mergeSort\n");
  mergeSort(AB,0,N-1);
  for (i=0;i<N;i++) {
        printf("(%d,%d)\n ",AB[i][0],AB[i][1]); 
  }
  free(AB);
//  free(S);
  return 0;
}

int** array2d(int i, int j) {
  int** array;
  int k;
  array = (int** )malloc(i*sizeof(int*));
  for (k=0;k<i;k++) {
        array[k] = (int* )malloc(j*sizeof(int));
  }
  return array;
}  

void merge(int** S, int low, int mid, int up) {
  int xmid = mid-low, xup = up-low,q;
  int i = 0, j = xmid+1, k = low;
  int** X = array2d(xup+1,2);
  if (X == NULL) exit(1);
  
  for (i=0;i<xup+1;i++) {
        X[i][0] = S[low+i][0];
        X[i][1] = S[low+i][1];
  }

  /*printf("\nX ");
  for (i=0;i<xup+1;i++) {
        printf("%d %d ",X[i][0],X[i][1]); 
  }
  printf("\n");*/
  
  i=0; //!!
  while ((i <= xmid) && (j <= xup)) {
    if (X[i][0] < X[j][0]) {S[k][0] = X[i][0]; S[k++][1] = X[i++][1];}
    else if (X[i][0] > X[j][0]) {S[k][0] = X[j][0]; S[k++][1] = X[j++][1];}
    else 
      if (X[i][1] > X[j][1]) {S[k][0] = X[j][0]; S[k++][1] = X[j++][1];}
    else {S[k][0] = X[i][0]; S[k++][1] = X[i++][1];}
  }
  
  if (i > xmid)
    for (q=j;q<=xup;q++){
          S[k][0] =  X[q][0]; S[k++][1] = X[q][1];
    }
  else
    for (q=i;q<=xmid;q++){
          S[k][0] =  X[q][0]; S[k++][1] = X[q][1];
    }     
  free(X);
}
    
void mergeSort(int** S, int left, int right) {
  int mid;
  if (left >= right) return;
  mid= (left+right)/2;
  mergeSort(S,left,mid);
  mergeSort(S,mid+1,right);
  merge(S,left,mid,right);
}

 
