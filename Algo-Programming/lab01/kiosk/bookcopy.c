/*Vlassi-Pandi Eirini (03108137)*/
/*        ex1 algo 2011-12      */


#include <stdlib.h>
#include <stdio.h>


int main() {
  long int N,K,sum=0,mo,po,ko,Amax=0,sol=-1,counter=0,flag;
  int i;

  scanf("%ld %ld",&N,&K);
  long int *A = calloc(N,sizeof(long int));
  if (A == NULL) exit(1);
  for (i=0;i<N;i++) {
        scanf("%ld/n",&A[i]);
        sum = sum + A[i];
        if (A[i] > Amax) {Amax = A[i];}
  }
 
  po = sum;
  
  if (sum%K > 0) {mo = sum/K +1;} else {mo = sum/K;}
  
  if (mo < Amax) {ko = Amax;}
  else {ko = mo;}

  while (ko<=po) {
     i=0;
     counter=0;
     
     if ((po+ko)%2 > 0) {sol = (po+ko)/2 +1;} else {sol = (po+ko)/2;}
           
     for(;;) {
        sum = 0;
        flag = 0;
        while (sum <= sol && i <= N-1) {
               sum = sum + A[i];
               if (i == N-1 && sum > sol) {flag = 1;}
               i++;
        }
        i--;
        counter++;
        if (i == (N-1)) {
            if ( flag == 1 && A[i] <= sol) { counter++;}
            break;
        }
     }

    if (counter <= K) {
        po = sol-1;
    }
    else{ 
        ko = sol+1;
    }     
  } 
  
  printf("%ld\n",ko);
  free(A);

  return 0;
}

