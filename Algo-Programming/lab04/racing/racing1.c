#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define BSIZE 1<<15

long int N,M,K,L,Ben,u,v,x,start,w,last,athroisma,result,i,km,out,stop,keep,ma,hs,p,r,max,mp,g,gg,tempos;
long int T[60000];
long int C[60000];
long long int d[60000];
long long int A[60000][3];
long long int B[60000];
long long int D[60000];
char buffer[BSIZE];
long bpos = 0L, bsize = 0L;

struct node 
{
    long long int prospoli;
    long long int km;
    struct node *next;  
};

struct node Akmes[60000];
struct node *temp;
struct node *temp2;

long readLong(); 
int compare(const void *, const void *); 
void insert(long long int, long long int); 
void indjk(long long int); 
void combine(long long int);
void constructHeap(long long int); 
void explore(long long int); 
void dijkstra(long long int); 

int main() {
    long long athroisma;
    int i, k;

    N = readLong();
    M = readLong();
    K = readLong();
    L = readLong();
    Ben = readLong();
   // scanf("%lld", &N);
   // scanf("%lld", &M);
   // scanf("%lld", &K);
   // scanf("%lld", &L);
   // scanf("%lld", &Ben);

    for (k=0;k<=N;k++) {
        Akmes[k].next = NULL;
        d[k] = -1;
    }

    for (i=0;i<M;++i) {

        u = readLong();
        v = readLong();
        x = readLong();
       // scanf("%lld", &u);
       // scanf("%lld", &v);
       // scanf("%lld", &x);

        temp=(struct node *)malloc(sizeof(struct node));
        temp->prospoli=v;
        temp->km=x;
        temp->next=Akmes[u].next;
        Akmes[u].next=temp;  /* pointing to the first node */

        temp=(struct node *)malloc(sizeof(struct node));
        temp->prospoli=u;
        temp->km=x;
        temp->next=Akmes[v].next;
        Akmes[v].next=temp;  /* pointing to the first node */

    }

    for (i=0;i<K;i++) { 
        T[i] = readLong();
       // scanf("%ld", &T[i]);
    }

    for (i=0;i<Ben;i++) { 
        C[i] = readLong();
        //scanf("%ld", &C[i]);
    }

    for (i=0;i<Ben;i++) { 
        temp=(struct node *)malloc(sizeof(struct node));
        temp->prospoli=C[i];
        temp->km=0;
        temp->next=Akmes[0].next;
        Akmes[0].next=temp;  /* pointing to the first node */
    }

    dijkstra(0);

    for (i=1;i<=(K-2);i++) { 
        D[i] = d[T[i]];
    }

    qsort(&D[1], K-2, sizeof(long long int), compare);

    athroisma = 0;

    for (i=0;i<(K-1);i++) { 
        temp = Akmes[T[i]].next;
        while (temp->prospoli != T[i+1]) {
            temp = temp->next;
        }
        athroisma += temp->km;
    }

    for (i=1; i<=L; ++i) {
        athroisma += D[i];
    }

    printf("%lld\n", athroisma);

    return 0;
}

long readLong() {
	long d = 0L, x = 0L;
	char c;

	while (1)  {
		if (bpos >= bsize) {
			bpos = 0;
			if (feof(stdin)) return x;
			bsize = fread(buffer, 1, BSIZE, stdin);
		}
		c = buffer[bpos++];
		if (c >= '0' && c <= '9') { x = x*10 + (c-'0'); d = 1; }
		else if (d == 1) return x;
	}
	return -1;
}

int compare(const void *a, const void *b) {
    long long c = *((long long *)a), d = *((long long *)b);

    if ( c < d ) return -1;
    if ( c == d ) return 0;
    return 1;
}

void insert(long long int k, long long int thesi) {

    long long int i,p;

    hs++;	
    A[hs][1] = k;
    A[hs][2] = thesi;
    B[thesi] = hs;
    i = hs; 
    p = i / 2;

    while ((i > 1) && (A[p][1] > A[i][1])) {
        tempos = A[p][1];
        A[p][1]=A[i][1];
        A[i][1] = tempos;
        tempos = A[p][2];
        A[p][2]=A[i][2];
        A[i][2] = tempos;	
        B[A[p][2]] = p;
        B[A[i][2]] = i;		
        i = p; 
        p = i / 2;
    } 
}

void indjk(long long int thesi) {

    long long int i,p;


    i = thesi; 
    p = i / 2;

    while ((i > 1) && (A[p][1] > A[i][1])) {
        tempos = A[p][1];
        A[p][1]=A[i][1];
        A[i][1] = tempos;
        tempos = A[p][2];
        A[p][2]=A[i][2];
        A[i][2] = tempos;	
        B[A[p][2]] = p;
        B[A[i][2]] = i;		
        i = p; 
        p = i / 2;
    } 
}

void combine(long long int i) {

    long long int l,mp,r;

    l = 2*i; r = 2*i+1; mp = i;
    if ((l <= hs) && (A[l][1] < A[mp][1]))
        mp = l;
    if ((r <= hs) && (A[r][1] < A[mp][1]))
        mp = r;
    if (mp != i) {
        tempos = A[i][1];
        A[i][1] = A[mp][1];
        A[mp][1] = tempos;
        tempos = A[i][2];
        A[i][2] = A[mp][2];
        A[mp][2] = tempos;
        B[A[mp][2]] = mp;
        B[A[i][2]] = i;
        combine(mp); 
    } 

}

void constructHeap(long long int n) {

    long long int i;	

    hs = n;
    for (i = n / 2; i > 0; i--)
        combine(i);
} 

long long int deleteMax(); 
long long int deleteMax() {

    assert(hs != 0);
    max = A[1][2];

    A[1][1] = A[hs][1];
    A[1][2]	= A[hs][2];
    B[A[hs][2]] = 1;
    hs--;
    if (hs != 0)
        combine(1);
    return(max); 
}

void explore(long long int komvos) {
    temp = Akmes[komvos].next;	
    while (temp != NULL) {
        if ( (temp->km + d[komvos] < d[temp->prospoli]) || (d[temp->prospoli] == -1) ) {
            keep = d[temp->prospoli];			
            d[temp->prospoli] = temp->km + d[komvos];
            if (keep == -1) {
                g = d[temp->prospoli];
                gg = temp->prospoli;
                insert(g, gg);
            }
            else {				
                A[B[temp->prospoli]][1] = d[temp->prospoli];
                indjk(B[temp->prospoli]);
            }
        }
        temp = temp->next;
    }
}	

void dijkstra(long long int s) {
    long long int q2;

    d[s] = 0;

    hs = 0;
    temp = Akmes[s].next;	

    while (temp != NULL) {

        if ( (temp->km < d[temp->prospoli]) || (d[temp->prospoli] == -1) ) {
            keep = d[temp->prospoli];
            d[temp->prospoli] = temp->km;

            if (keep == -1) {
                hs++;
                A[hs][1] = d[temp->prospoli];
                A[hs][2] = temp->prospoli;
                B[temp->prospoli] = hs; 
            }
            else {				
                A[B[temp->prospoli]][1] = d[temp->prospoli];
            }

        }

        temp = temp->next;
    }

    constructHeap(hs);	
    q2 = deleteMax();

    while (1) {				
        explore(q2);
        if ( hs == 0 ) break;
        q2 = deleteMax();
    }
}



