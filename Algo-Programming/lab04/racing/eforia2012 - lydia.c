#include <stdio.h>
#include <stdlib.h>
#define BSIZE 1<<15

char buffer[BSIZE];
long bpos = 0L, bsize = 0L;

long readLong() {
	long d = 0L, x = 0L;
	char c;

	while (1)  {
		if (bpos >= bsize) {
			bpos = 0;
			if (feof(stdin)) return x;
			bsize = fread(buffer, 1, BSIZE, stdin);
		}
		c = buffer[bpos++];
		if (c >= '0' && c <= '9') { x = x*10 + (c-'0'); d = 1; }
		else if (d == 1) return x;
	}
	return -1;
}

long** make2darray(long x, long y){
	long** myarray;
	long l;
	myarray = (long** )malloc(x*sizeof(long*));
	for (l=0;l<x;l++){
		myarray[l] = (long* )malloc(y*sizeof(long));
	}
	return myarray;
}

/*void printD() {
	int i;
	for (i = 0; i < N; ++i)
		printf("%10d", i);
	printf("\n");
	for (i = 0; i < N; ++i) {
		printf("%10ld", d[i]);
	}
	printf("\n");
}*/

long M; /* The number of nonzero edges in the graph */
long N; /* The number of nodes in the graph */
long** Adj;

unsigned long long dijkstra(long s, long dest) {
	long i, k, mini;
	unsigned long long INFINITY = 1<<31;
	
	long* visited = calloc(N, sizeof(long));
	if (visited == NULL) exit(1);
	unsigned long long* d = malloc(N*sizeof(long long));
	if (d == NULL) exit(1);
	
	for (i=0;i<N;++i) {
		d[i] = INFINITY;
		visited[i] = 0; /* the i-th element has not yet been visited */
	}

	d[s] = 0;

	for (k=0; k<N; ++k) {
		mini = -1;
		for (i=0; i<N; ++i)
			if (!visited[i] && ((mini == -1) || (d[i] < d[mini])))
				mini = i;

		visited[mini] = 1;

		for (i=0; i<N; ++i) 
			if (Adj[mini][i])
				if (d[mini] +(unsigned long long)Adj[mini][i] < d[i]) 
					d[i] = d[mini] +(unsigned long long)Adj[mini][i];
	}
	//for (i = 0; i < N; ++i) {
	//	printf("%lld", d[i]);
	//}
	//printf("\n");
	return d[dest];
}

int main(){

	long i, j, nd1, nd2, c;
	unsigned long long sum=0, dpath, e9;
	long aw[2];
	N = readLong();
	M = readLong();
	
	Adj = make2darray(N, N);
	if (Adj == NULL) exit(1);

	long* cnt = calloc(N, sizeof(long));
	if (cnt == NULL) exit(1);
	
	for (i=0;i<N;i++) {
		for (j=0;j<N;j++) {
			Adj[i][j] = 20001;
			if (i == j) {
				Adj[i][i] = 0;
			}	
		}
	}
	for (i=0;i<M;i++) {
		nd1 = readLong(); 
		nd2 = readLong(); 
		c   = readLong(); 
		sum += c;
		cnt[nd1-1]++; cnt[nd2-1]++;	
		if (Adj[nd1-1][nd2-1] > c) {
				Adj[nd1-1][nd2-1] = c;
				Adj[nd2-1][nd1-1] = c;
		}
	}
	j = 0;
	for (i=0;i<N;i++) {
		if ((cnt[i]%2) != 0) {
			aw[j++] = i;
		}	
		if (j == 2) break;
	}
				
	printf("%ld %ld \n", aw[0], aw[1]);
//	for (i=0;i<N;i++) {
//		for (j=0;j<N;j++) {
//			printf("%ld ", Adj[i][j]);
//		}
//		printf("\n");
//	}
	printf("sum : %lld\n", sum);

	dpath = dijkstra(aw[0], aw[1]);
	printf("pisw : %lld\n", dpath);
	e9 = sum + dpath;
	printf("e9 : %lld\n", e9);
	return 0;
}
