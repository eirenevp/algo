#!/bin/bash
gcc kiosk.c -o kiosk &&
for i in {1..31}
do
    echo "Running test $i..."
    ./kiosk < "input$i.txt" > "myoutput$i.txt"
    diff -q "myoutput$i.txt" "output$i.txt" 
done

