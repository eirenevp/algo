#define MODLIMIT 100000007

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <iostream>
#include <iterator>
#include <algorithm>

using namespace std;

void multiply(unsigned long long int **mat0, unsigned long long int **mat2, unsigned long long int **mat3, int N) {
    unsigned long long int buf;
    for (int i=0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
            buf = 0;
            for(int k = 0; k < N; k ++) {
                buf += mat0[i][k]*mat2[k][j];
            }
                mat3[i][j] = buf % MODLIMIT;
        }
    }
}

void solvem(unsigned long long int **initial_matrix, int N, int k,int s,int t) {
    unsigned long long int **sup1,**sup2,**ans;
    sup1 = initial_matrix;
    sup2 = new unsigned long long int*[N];
    ans = new unsigned long long int*[N];
    unsigned int bit;
    for (int i = 0; i < N; i++) {
        ans[i] = new unsigned long long int[N];
        fill(ans[i],ans[i]+N,0);
        ans[i][i]=1;
        sup2[i] = new unsigned long long int[N];
    }
    k--;
    for (int i = 0 ,j = 0;i < 32; ++i) {
        bit = ( k & ( 1 << i ) ) >> i;
        if( bit ) {
            for( ; j < i; ++j) {
                multiply(sup1 ,sup1 ,sup2 ,N);
                swap(sup1 ,sup2);
            }
            multiply(ans ,sup1 ,sup2 ,N);
            swap(sup2, ans);
        }
    }
    printf("%llu\n", ans[s-1][t-1]);
    delete [] sup1;
    delete [] sup2;
}



int main() {
    int k, N, M, s, t, a, b;
    unsigned long long int **mat;
    int nothing = scanf("%d %d %d %d %d", &k, &N, &M, &s, &t);

    mat = new unsigned long long int*[N];
    for(int i = 0; i < N; i ++) {
        mat[i] = new unsigned long long int[N];
        fill(mat[i], mat[i]+N, 0);
    }
    for (int i = 0; i < M; ++i) {
        nothing = scanf("%d %d", &a, &b);
        a--;
        b--;
        mat[a][b] = 1;
    }
    solvem(mat, N, k, s, t);
    return 0;
}
